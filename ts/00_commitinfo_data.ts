/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartdelay',
  version: '3.0.5',
  description: 'timeouts for the async/await era, written in TypeScript'
}
